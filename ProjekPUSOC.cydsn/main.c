/* ========================================
 * Copyright Mateusz Gutowski, 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 * ========================================
*/


#include "project.h"

//wszystkie czasy w us
#define NEC_AGC_TIME 9000
#define NEC_AFTER_AGC_TIME 4500
#define NEC_AFTER_AGC_TOL 100
#define NEC_INIT_TIME 560
#define NEC_LOGIC_0_END_TIME 1120 - 560
#define NEC_LOGIC_1_END_TIME 2250 - 560
#define NEC_LOGIC_0_END_TOL 150
#define NEC_LOGIC_1_END_TOL 150
//tolerancja na inicjalizację bitu (w protokole 560us całość inicjalizacji):
#define NEC_INIT_TOL 150

#define NEC_FAILITURE -1
#define NEC_BIT_AMOUNT 32

#define TIMER1_MAX_TIME 2731
#define TIMER1_MAX_COUNTER 65536
#define TIMER1_COUNTER_FOR_1us 24
#define TIMER1_LIMIT_OVR_COUNTER 1000

//wartości licznika przeładowań timera1 dla których wykonywane są akcje:
#define NEXT_DATA_ALLOW_OVR_COUNTER 100
#define NEXT_LCD_REFRESH_OVR_COUNTER 100

#define HIGH 1
#define LOW 0

#define TRUE 1
#define FALSE 0

uint8 ir_received = FALSE;
int ir_receiving = 0;
uint8 allowNextData = TRUE;
int timer1OVRcounter = 0;
int counter = 0;
int deviceNumber = -1;

uint8 nec_logic(int time_init, int time_end)
{
    //jeśli czasy inicjalizacji się zgadzają:
   if(time_init > NEC_INIT_TIME - NEC_INIT_TOL && 
      time_init < NEC_INIT_TIME + NEC_INIT_TOL)
   {
    //czy czasy dla logicznego 0 się zgadzają:
    if(time_end > NEC_LOGIC_0_END_TIME - NEC_LOGIC_0_END_TOL &&
       time_end < NEC_LOGIC_0_END_TIME + NEC_LOGIC_0_END_TOL)return 0;
    //czy czasy dla logicznej 1 się zgadzają:
   /* if(time_end > NEC_LOGIC_1_END_TIME - NEC_LOGIC_1_END_TOL &&
       time_end < NEC_LOGIC_1_END_TIME + NEC_LOGIC_1_END_TOL)return 1;*/
    }
    return 1;
}
void endIRreceive(void)
{
    ir_receiving = FALSE;
    allowNextData = FALSE;
}
uint32 nec_init()
{
    int counterNow = 0, counterStart = Timer_1_ReadCounter();
    int time = 0;
    uint32 IR_data = 0;
    timer1OVRcounter = 0;
    Timer_1_Start();
    int8 bitNo = NEC_BIT_AMOUNT;

    while(IR_PIN_Read() == HIGH)//czekanie na przeminięcie impulsu początkowego
    {
        if(timer1OVRcounter > NEC_AFTER_AGC_TIME/TIMER1_MAX_TIME + 2)//jeśli znacznie przekroczono czas impulsu
        {
            endIRreceive();
            return -9;//wyjdź, błąd ramki
        }
    }
    
    counterNow = Timer_1_ReadCounter();
    time = (counterStart - counterNow + 
            TIMER1_MAX_COUNTER * timer1OVRcounter)/TIMER1_COUNTER_FOR_1us;//czas trwania impulsu
    if (!(time >= NEC_AFTER_AGC_TIME - NEC_AFTER_AGC_TOL &&
        time <= NEC_AFTER_AGC_TIME + NEC_AFTER_AGC_TOL))//jeśli impuls nie zmieścił się w przedzale czasowym
    {
        endIRreceive();
        return -8;
    }
    //odbieranie właściwych bitów(początek od najstarszego):
    bitNo = NEC_BIT_AMOUNT-1;
    int time_init = 0, time_end = 0;
    while(bitNo >= 0)
    {
        counterStart = Timer_1_ReadCounter();
        timer1OVRcounter = 0;    
        while(IR_PIN_Read() == LOW)//czas init bitu
        {
            if(timer1OVRcounter >= 2)//jeśli znacznie przekroczono czas impulsu
            {
                endIRreceive();
                return -7;
            }
        }
        counterNow = Timer_1_ReadCounter();
        time_init = (counterStart - counterNow + 
            TIMER1_MAX_COUNTER * timer1OVRcounter)/TIMER1_COUNTER_FOR_1us;//czas trwania init bitu
        
        counterStart = Timer_1_ReadCounter();
        timer1OVRcounter = 0;    
        while(IR_PIN_Read() == HIGH)//czas reszty bitu
        {
            if(timer1OVRcounter >= 2)//jeśli znacznie przekroczono czas impulsu
            {
                endIRreceive();
                return -6;
            }
        }
        counterNow = Timer_1_ReadCounter();
        time_end = (counterStart - counterNow + 
            TIMER1_MAX_COUNTER * timer1OVRcounter)/TIMER1_COUNTER_FOR_1us;//czas trwania reszty bitu
        uint8 bit = nec_logic(time_init,time_end);
        
        IR_data += (bit<<bitNo);//uzyskany bit z zerowej pozycji przesuń na koniecczną
        bitNo--;
    }
    ir_received = TRUE;
    endIRreceive();
    return IR_data;
   
}
CY_ISR(IR_ISR)
{
    if (ir_receiving == FALSE && allowNextData == TRUE)
    {
        ir_receiving = TRUE;
        ir_received = FALSE;
    }
}

CY_ISR(Timer_1_OVR_ISR)
{
    timer1OVRcounter++;
    if(timer1OVRcounter > NEXT_DATA_ALLOW_OVR_COUNTER) allowNextData = TRUE;
    if(timer1OVRcounter > TIMER1_LIMIT_OVR_COUNTER) timer1OVRcounter = 0;
}
void recognizeCommand(uint32_t command)
{
    const uint32_t address = 0xE0E0 << 16;
    const uint32_t button_N[10] = {0x8877, 0x20DF,//0,1
                                   0xA05F, 0x609F,//2,3
                                   0x10EF, 0x906F,//4,5
                                   0x50AF, 0x30CF,//6,7
                                   0xB04F, 0x708F};//8,9

    deviceNumber = -1;
    for (uint8_t i = 0; i < 10; i++)
    {
        if (command == address + button_N[i])
        {
            //przełączanie diod:
            if (i == 0) LED4_Write(!LED4_Read());
            else if (i == 1) LED3_Write(!LED3_Read());   
            //numer urządzenia:
            deviceNumber = i;    
        }
    }
    
 
    
}
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    IR_INT_StartEx(IR_ISR);
    Timer_1_OVR_StartEx(Timer_1_OVR_ISR);
    
    LCD_Char_1_Start();
    Timer_1_Start();
    
    

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    int nec_out = NEC_FAILITURE;
    for(;;)
    {
       
        if(ir_receiving)
        {
            nec_out = nec_init();
            recognizeCommand(nec_out);
        }
        if(timer1OVRcounter % NEXT_LCD_REFRESH_OVR_COUNTER == 0)
        {
            LCD_Char_1_ClearDisplay();
            LCD_Char_1_Position(0,0);
            LCD_Char_1_PrintInt32(nec_out);
            LCD_Char_1_Position(1,0);
            if(deviceNumber != -1) 
            {
                LCD_Char_1_PrintString("Urzadzenie:");
                LCD_Char_1_PrintNumber(deviceNumber);
            }
            else
            {
                LCD_Char_1_PrintString("Nieobslugiwane");
            }
        }
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
