/*******************************************************************************
* File Name: outPIN.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_outPIN_H) /* Pins outPIN_H */
#define CY_PINS_outPIN_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "outPIN_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 outPIN__PORT == 15 && ((outPIN__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    outPIN_Write(uint8 value);
void    outPIN_SetDriveMode(uint8 mode);
uint8   outPIN_ReadDataReg(void);
uint8   outPIN_Read(void);
void    outPIN_SetInterruptMode(uint16 position, uint16 mode);
uint8   outPIN_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the outPIN_SetDriveMode() function.
     *  @{
     */
        #define outPIN_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define outPIN_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define outPIN_DM_RES_UP          PIN_DM_RES_UP
        #define outPIN_DM_RES_DWN         PIN_DM_RES_DWN
        #define outPIN_DM_OD_LO           PIN_DM_OD_LO
        #define outPIN_DM_OD_HI           PIN_DM_OD_HI
        #define outPIN_DM_STRONG          PIN_DM_STRONG
        #define outPIN_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define outPIN_MASK               outPIN__MASK
#define outPIN_SHIFT              outPIN__SHIFT
#define outPIN_WIDTH              1u

/* Interrupt constants */
#if defined(outPIN__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in outPIN_SetInterruptMode() function.
     *  @{
     */
        #define outPIN_INTR_NONE      (uint16)(0x0000u)
        #define outPIN_INTR_RISING    (uint16)(0x0001u)
        #define outPIN_INTR_FALLING   (uint16)(0x0002u)
        #define outPIN_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define outPIN_INTR_MASK      (0x01u) 
#endif /* (outPIN__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define outPIN_PS                     (* (reg8 *) outPIN__PS)
/* Data Register */
#define outPIN_DR                     (* (reg8 *) outPIN__DR)
/* Port Number */
#define outPIN_PRT_NUM                (* (reg8 *) outPIN__PRT) 
/* Connect to Analog Globals */                                                  
#define outPIN_AG                     (* (reg8 *) outPIN__AG)                       
/* Analog MUX bux enable */
#define outPIN_AMUX                   (* (reg8 *) outPIN__AMUX) 
/* Bidirectional Enable */                                                        
#define outPIN_BIE                    (* (reg8 *) outPIN__BIE)
/* Bit-mask for Aliased Register Access */
#define outPIN_BIT_MASK               (* (reg8 *) outPIN__BIT_MASK)
/* Bypass Enable */
#define outPIN_BYP                    (* (reg8 *) outPIN__BYP)
/* Port wide control signals */                                                   
#define outPIN_CTL                    (* (reg8 *) outPIN__CTL)
/* Drive Modes */
#define outPIN_DM0                    (* (reg8 *) outPIN__DM0) 
#define outPIN_DM1                    (* (reg8 *) outPIN__DM1)
#define outPIN_DM2                    (* (reg8 *) outPIN__DM2) 
/* Input Buffer Disable Override */
#define outPIN_INP_DIS                (* (reg8 *) outPIN__INP_DIS)
/* LCD Common or Segment Drive */
#define outPIN_LCD_COM_SEG            (* (reg8 *) outPIN__LCD_COM_SEG)
/* Enable Segment LCD */
#define outPIN_LCD_EN                 (* (reg8 *) outPIN__LCD_EN)
/* Slew Rate Control */
#define outPIN_SLW                    (* (reg8 *) outPIN__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define outPIN_PRTDSI__CAPS_SEL       (* (reg8 *) outPIN__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define outPIN_PRTDSI__DBL_SYNC_IN    (* (reg8 *) outPIN__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define outPIN_PRTDSI__OE_SEL0        (* (reg8 *) outPIN__PRTDSI__OE_SEL0) 
#define outPIN_PRTDSI__OE_SEL1        (* (reg8 *) outPIN__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define outPIN_PRTDSI__OUT_SEL0       (* (reg8 *) outPIN__PRTDSI__OUT_SEL0) 
#define outPIN_PRTDSI__OUT_SEL1       (* (reg8 *) outPIN__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define outPIN_PRTDSI__SYNC_OUT       (* (reg8 *) outPIN__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(outPIN__SIO_CFG)
    #define outPIN_SIO_HYST_EN        (* (reg8 *) outPIN__SIO_HYST_EN)
    #define outPIN_SIO_REG_HIFREQ     (* (reg8 *) outPIN__SIO_REG_HIFREQ)
    #define outPIN_SIO_CFG            (* (reg8 *) outPIN__SIO_CFG)
    #define outPIN_SIO_DIFF           (* (reg8 *) outPIN__SIO_DIFF)
#endif /* (outPIN__SIO_CFG) */

/* Interrupt Registers */
#if defined(outPIN__INTSTAT)
    #define outPIN_INTSTAT            (* (reg8 *) outPIN__INTSTAT)
    #define outPIN_SNAP               (* (reg8 *) outPIN__SNAP)
    
	#define outPIN_0_INTTYPE_REG 		(* (reg8 *) outPIN__0__INTTYPE)
#endif /* (outPIN__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_outPIN_H */


/* [] END OF FILE */
